using System.Linq;
using PlaylistAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace PlaylistAPI.Business
{
    public class BaseBusiness<TModel> where TModel : BaseModel
    {
        private PlaylistContext Context { get; set; }
        protected DbSet<TModel> Table { get; }

        public BaseBusiness(PlaylistContext context)
        {
            Context = context;
            Table = Context.AcquireDbSet<TModel>();
        }

        public virtual TModel Get(int id)
        {
            if (Table != null)
            {
                return Table.Where(item => item.Id == id).FirstOrDefault();
            }
            return null;
        }

        public virtual TModel Insert(TModel model)
        {
            if (Table != null)
            {
                try
                {
                    Table.Add(model);
                    Context.SaveChanges();
                    return model;
                }
                catch { return null; }
            }
            return null;
        }

        public virtual TModel Update(TModel model)
        {
            if (Table != null)
            {
                try
                {
                    Table.Update(model);
                    Context.SaveChanges();
                    return model;
                }
                catch { return null; }
            }
            return null;
        }

        public virtual TModel Delete(int id)
        {
            if (Table != null)
            {
                try
                {
                    var model = Table.Where(item => item.Id == id).FirstOrDefault() as TModel;
                    if (model != null)
                    {
                        Table.Remove(model);
                        Context.SaveChanges();
                        return model;
                    }
                    else return null;
                }
                catch { return null; }
            }
            return null;
        }
    }
}