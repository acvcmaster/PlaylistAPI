using System;
using System.Linq;
using PlaylistAPI.Models;
using PlaylistAPI.Services;

namespace PlaylistAPI.Business
{
    public class SessionBusiness : BaseBusiness<Session>
    {
        private UserBusiness UserBusiness { get; }
        private IGuidService GuidService { get; }

        public SessionBusiness(
            PlaylistContext context,
            UserBusiness userBusiness,
            IGuidService guidService
        ) : base(context)
        {
            UserBusiness = userBusiness;
            GuidService = guidService;
        }

        public override Session Insert(Session model)
        {
            var user = UserBusiness.UserByName(model.UserName);
            try
            {
                if (user != null)
                {
                    if (user.Password == model.Password)
                    {
                        model.Guid = GuidService.GetGuid();
                        model.UserId = user.Id;
                        model.Expires = DateTime.Now.AddDays(2);
                        var result = base.Insert(model);
                        return result;
                    }
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        public Session Validate(Session model)
        {
            var user = UserBusiness.UserByName(model.UserName);
            if (user != null)
            {
                try
                {
                    var session = (
                        from s in Table
                        where s.Guid == model.Guid &&
                            s.UserName == model.UserName &&
                            DateTime.Now < s.Expires
                        select s
                    ).FirstOrDefault();

                    if (session != null)
                        session.Password = user.Password;
                    return session;
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        public Session Login(Session model)
        {
            if (model.Guid != null)
                return Validate(model);
            else return Insert(model);
        }
    }
}