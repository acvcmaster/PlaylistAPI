using PlaylistAPI.Models;

namespace PlaylistAPI.Business
{
    public class HardCodedEntryBusiness : BaseBusiness<HardCodedEntry>
    {
        public HardCodedEntryBusiness() : base(null)
        {
        }
    }
}