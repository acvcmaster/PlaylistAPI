using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PlaylistAPI.Models;

namespace PlaylistAPI.Business
{
    public class PlaylistRuleBusiness : BaseBusiness<PlaylistRule>
    {
        private PlaylistContext Context { get; }
        private DbSet<Rule> RuleTable { get; }
        private DbSet<Property> PropertyTable { get; }
        private DbSet<Comparator> ComparatorTable { get; }

        public PlaylistRuleBusiness(PlaylistContext context) : base(context)
        {
            Context = context;
            RuleTable = Context.AcquireDbSet<Rule>();
            PropertyTable = Context.AcquireDbSet<Property>();
            ComparatorTable = Context.AcquireDbSet<Comparator>();
        }

        public IEnumerable<PlaylistRuleCompleteModel> GetPlaylistRules(int id)
        {
            return (from rule in Table
                    join r in RuleTable on rule.RuleId equals r.Id
                    join p in PropertyTable on r.PropertyId equals p.Id
                    join c in ComparatorTable on r.ComparatorId equals c.Id
                    where rule.PlaylistId == id
                    orderby rule.Id
                    select new PlaylistRuleCompleteModel()
                    {
                        Id = rule.Id,
                        Creation = rule.Creation,
                        LastModification = rule.LastModification,
                        PropertyId = p.Id,
                        Property = p.Name,
                        PropertyDescription = p.Description,
                        PropertyType = p.Type,
                        Operator = c.Operator,
                        OperatorDescription = c.Description,
                        Data = rule.Data
                    });
        }
    }
}