using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PlaylistAPI.Models;
using PlaylistsNET.Content;
using PlaylistsNET.Models;

namespace PlaylistAPI.Business
{
    public class PlaylistBusiness : BaseBusiness<Playlist>
    {
        private PlaylistContext Context { get; }
        private DbSet<SongProperty> SongPropertyTable { get; }
        private DbSet<Property> PropertyTable { get; }
        private DbSet<HardCodedEntry> HardCodedEntryTable { get; }
        private PlaylistRuleBusiness PlaylistRuleBusiness { get; }
        private SongBusiness SongBusiness { get; }

        public PlaylistBusiness(
            PlaylistContext context,
            PlaylistRuleBusiness playlistRuleBusiness,
            SongBusiness songBusiness
        ) : base(context)
        {
            Context = context;

            SongPropertyTable = Context.AcquireDbSet<SongProperty>();
            PropertyTable = Context.AcquireDbSet<Property>();
            HardCodedEntryTable = Context.AcquireDbSet<HardCodedEntry>();

            PlaylistRuleBusiness = playlistRuleBusiness;
            SongBusiness = songBusiness;
        }

        public IEnumerable<Playlist> GetAllFromUser(int Id)
        {
            return Table.Where(item => item.OwnerID == Id);
        }

        public IEnumerable<Playlist> GetAll()
        {
            return Table;
        }

        public IEnumerable<CompleteSong> GetSongs(int id, HttpRequest request)
        {
            try
            {
                var playlistRules = PlaylistRuleBusiness.GetPlaylistRules(id).ToList();
                var playlist = base.Get(id);

                Dictionary<int, IEnumerable<CompleteSong>> auxiliaryPlaylists = new Dictionary<int, IEnumerable<CompleteSong>>();

                foreach (var rule in playlistRules)
                    if (rule.Operator == "i" || rule.Operator == "!i")
                        auxiliaryPlaylists.Add(int.Parse(rule.Data), GetSongs(int.Parse(rule.Data), request));

                var songsAndRules = playlist.IsSmart ? (from rule in playlistRules
                                                        join sp in SongPropertyTable on rule.PropertyId equals sp.PropertyId
                                                        join p in PropertyTable on sp.PropertyId equals p.Id
                                                        where (p.Type == rule.PropertyType) && PropertyAccordingToRule(rule.PropertyType, rule.Operator,
                                                            sp.Value, rule.Data, sp.SongId, auxiliaryPlaylists)
                                                        select new { songId = sp.SongId, ruleId = rule.Id }).ToList() : null;


                List<CompleteSong> completeSongs = null;
                if (songsAndRules != null)
                {
                    Dictionary<int, int> songsAndRulesDict = new Dictionary<int, int>();
                    foreach (var item in songsAndRules)
                    {
                        if (!songsAndRulesDict.ContainsKey(item.songId))
                            songsAndRulesDict.Add(item.songId, 0);

                        songsAndRulesDict[item.songId]++;
                    }

                    var ids = songsAndRules != null ?
                    (
                        from item in songsAndRulesDict
                        where (playlist.DisjunctiveRules && item.Value > 0) || (!playlist.DisjunctiveRules && item.Value == playlistRules.Count)
                        orderby item.Key
                        select item.Key
                    ).ToList() : null;

                    if (ids != null)
                        completeSongs = SongBusiness.GetSongsFromIds(ids, request);
                }
                else
                {
                    var ids = (from e in HardCodedEntryTable where e.PlaylistId == id select e.SongId).Distinct().ToList();
                    completeSongs = SongBusiness.GetSongsFromIds(ids, request);
                }
                return completeSongs;
            }
            catch { return null; }
        }

        public PlaylistFile GetPlaylistFile(int id, HttpRequest request)
        {
            try
            {
                var basePlaylist = base.Get(id);
                if (basePlaylist == null)
                    return null;

                MemoryStream playlistStream = new MemoryStream();
                M3uPlaylist playlist = new M3uPlaylist() { IsExtended = true };
                var songs = GetSongs(id, request);

                foreach (var song in songs)
                {
                    playlist.PlaylistEntries.Add(new M3uPlaylistEntry()
                    {
                        Album = song.Properties.Where(item => item.Name == "ALBUM").FirstOrDefault()?.Value,
                        AlbumArtist = song.Properties.Where(item => item.Name == "ALBUM_ARTIST").FirstOrDefault()?.Value,
                        Path = song.RemoteUrl,
                        Title = $"{song.Properties.Where(item => item.Name == "NAME").FirstOrDefault()?.Value}"
                    });
                }
                var playlistText = PlaylistToTextHelper.ToText(playlist);
                var data = Encoding.Default.GetBytes(playlistText);
                return new PlaylistFile() { Data = data, Playlist = basePlaylist };
            }
            catch { return null; }
        }

        #region Helpers
        private bool PropertyAccordingToRule(string propertyType, string op, string value, string data, int songId, Dictionary<int, IEnumerable<CompleteSong>> auxiliaryPlaylists)
        {
            try
            {
                if (value != null || propertyType == "PLAYLIST")
                {
                    switch (propertyType)
                    {
                        case "STRING":
                            return CompareStrings(value, data, op);
                        case "DATETIME":
                            return CompareDatetimes(DateTime.Parse(value), DateTime.Parse(data), op);
                        case "INTEGER":
                            return CompareIntegers(int.Parse(value), int.Parse(data), op);
                        case "BOOLEAN":
                            return CompareBooleans(bool.Parse(value), bool.Parse(data), op);
                        case "PLAYLIST":
                            {
                                var contains = auxiliaryPlaylists[int.Parse(data)].Where(item => item.Song.Id == songId).FirstOrDefault() != null;
                                return op == "==" ? contains : !contains;
                            }
                    }
                }
                return false;
            }
            catch { return false; }
        }

        private bool CompareStrings(string a, string b, string op)
        {
            switch (op)
            {
                case "==":
                    return a.Equals(b);
                case "!=":
                    return !a.Equals(b);
                case "c":
                    return a.Contains(b);
                case "!c":
                    return !a.Contains(b);
            }
            return false;
        }

        private bool CompareDatetimes(DateTime a, DateTime b, string op)
        {
            switch (op)
            {
                case "==":
                    return a.Equals(b);
                case "!=":
                    return !a.Equals(b);
                case "b":
                    return (b - a).Ticks > 0;
                case "!b":
                    return (b - a).Ticks < 0;
            }
            return false;
        }

        private bool CompareIntegers(int a, int b, string op)
        {
            switch (op)
            {
                case ">":
                    return a > b;
                case ">=":
                    return a >= b;
                case "<":
                    return a < b;
                case "<=":
                    return a <= b;
                case "==":
                    return a.Equals(b);
                case "!=":
                    return !a.Equals(b);
            }
            return false;
        }

        private bool CompareBooleans(bool a, bool b, string op)
        {
            switch (op)
            {
                case "t":
                    return a;
                case "!t":
                    return !a;
            }
            return false;
        }
        #endregion
    }
}