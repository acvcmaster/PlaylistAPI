using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using PlaylistAPI.Models;
using PlaylistAPI.Services;

namespace PlaylistAPI.Business
{
    public class SongBusiness : BaseBusiness<Song>
    {
        private PlaylistContext Context { get; }
        private IThumbnailingService ThumbnailingService { get; }
        private FileSystemCrawlerService CrawlerService { get; }
        private HttpCrawlerService HttpCrawlerService { get; }
        private DbSet<SongProperty> SongPropertyTable { get; }
        private DbSet<Property> PropertyTable { get; }

        public SongBusiness(
            PlaylistContext context,
            IThumbnailingService thumbnailingService,
            FileSystemCrawlerService crawlerService,
            HttpCrawlerService httpCrawlerService
        ) : base(context)
        {
            Context = context;
            ThumbnailingService = thumbnailingService;
            CrawlerService = crawlerService;
            HttpCrawlerService = httpCrawlerService;

            SongPropertyTable = Context.AcquireDbSet<SongProperty>();
            PropertyTable = Context.AcquireDbSet<Property>();
        }

        public override Song Insert(Song model)
        {
            var result = QuickInsert(model);
            Context.SaveChanges();
            return result;
        }

        public Song QuickInsert(Song model)
        {
            Uri url;
            bool urlSuccess = Uri.TryCreate(model.Url, UriKind.Absolute, out url);
            Song song = base.Insert(model);

            try
            {
                if (urlSuccess)
                {
                    if (song == null)
                        return null;

                    string contentType = new FileExtensionContentTypeProvider().TryGetContentType(url.AbsoluteUri, out contentType) ?
                        contentType : "application/octet-stream";

                    if (!contentType.StartsWith("audio/"))
                        throw new FormatException();

                    var file = url.Scheme == Uri.UriSchemeFile ? TagLib.File.Create(song.Url) :
                        TagLib.File.Create(new TaglibFile(url.AbsoluteUri, url), null, TagLib.ReadStyle.None);

                    var properties = (from item in PropertyTable select item).ToList();
                    var songProperties = new List<SongProperty>();

                    foreach (var property in properties)
                        songProperties.Add(GetSongProperty(song, property, file));

                    SongPropertyTable.AddRange(songProperties);
                    return song;
                }
                else throw new UriFormatException();
            }
            catch
            {
                if (song != null)
                    base.Delete(song.Id);
            }

            return null;
        }

        public IEnumerable<Song> MassInsert(string directoryUrl)
        {
            var uri = new Uri(directoryUrl);
            IEnumerable<string> files = null;
            if (uri.Scheme == Uri.UriSchemeFile)
                files = this.CrawlerService.ListFiles(directoryUrl);
            else if (uri.Scheme == Uri.UriSchemeHttp)
                files = this.HttpCrawlerService.ListFiles(directoryUrl);

            if (files != null && files.Count() > 0)
            {
                List<Song> insertedSongs = new List<Song>();
                foreach (var file in files)
                {
                    var song = QuickInsert(new Song() { Url = file });
                    if (song != null)
                        insertedSongs.Add(song);
                }
                return insertedSongs;
            }
            return null;
        }

        public Stream GetCoverArt(int id)
        {
            var song = Get(id);
            if (song != null)
                return ThumbnailingService.GetThumbnail(song);
            return null;
        }

        public IEnumerable<CompleteSong> GetAll(int page, int entriesPerPage, HttpRequest request)
        {
            page = page != 0 ? page : 1;
            entriesPerPage = entriesPerPage != 0 ? entriesPerPage : int.MaxValue;
            try
            {
                var songs = (from song in Table orderby song.Id select song)
                    .Skip(entriesPerPage * (page - 1))
                    .Take(entriesPerPage)
                    .ToDictionary(item => item.Id);
                var songIds = songs.Select(item => item.Key);

                var properties = (from property in SongPropertyTable
                                  join p in PropertyTable on property.PropertyId equals p.Id
                                  where songIds.Contains(property.SongId)
                                  select new CompleteSongProperty
                                  {
                                      Id = property.Id,
                                      Creation = property.Creation,
                                      LastModification = property.LastModification,
                                      PropertyId = p.Id,
                                      Name = p.Name,
                                      Type = p.Type,
                                      Description = p.Description,
                                      SongId = property.SongId,
                                      Value = property.Value
                                  }).ToList();


                List<CompleteSong> result = new List<CompleteSong>();
                foreach (var songPair in songs)
                {
                    string contentType = new FileExtensionContentTypeProvider().TryGetContentType(songPair.Value.Url, out contentType) ?
                        contentType : "application/octet-stream";

                    CompleteSong completeSong = new CompleteSong()
                    {
                        Song = songPair.Value,
                        Type = contentType,
                        Properties = properties.Where(item => item.SongId == songPair.Value.Id)
                    };

                    var songUrl = new Uri(songPair.Value.Url);

                    if (request != null)
                        completeSong.RemoteUrl = songUrl.Scheme == Uri.UriSchemeFile ? $"{request.Scheme}://{request.Host}/Song/GetFile?id={songPair.Value.Id}" : songUrl.AbsoluteUri;
                    result.Add(completeSong);
                }
                return result;
            }
            catch { return null; }
        }

        public List<CompleteSong> GetSongsFromIds(List<int> ids, HttpRequest request)
        {
            var completeSongs = new List<CompleteSong>();

            var songs = from song in Table where ids.Contains(song.Id) select song;

            var properties = (from property in SongPropertyTable
                              join p in PropertyTable on property.PropertyId equals p.Id
                              where ids.Contains(property.SongId)
                              select new CompleteSongProperty
                              {
                                  Id = property.Id,
                                  Creation = property.Creation,
                                  LastModification = property.LastModification,
                                  PropertyId = p.Id,
                                  Name = p.Name,
                                  Type = p.Type,
                                  Description = p.Description,
                                  SongId = property.SongId,
                                  Value = property.Value
                              }).ToList();

            foreach (var song in songs)
            {

                Uri url = new Uri(song.Url);
                if (url.Scheme == Uri.UriSchemeFile)
                    if (!File.Exists(song.Url))
                        continue;

                string contentType = new FileExtensionContentTypeProvider().TryGetContentType(url.AbsoluteUri, out contentType) ?
                    contentType : "application/octet-stream";

                var completeSong = new CompleteSong() { Song = song, Properties = properties.Where(item => item.SongId == song.Id).ToList(), Type = contentType };

                if (request != null)
                    completeSong.RemoteUrl = url.Scheme == Uri.UriSchemeFile ? $"{request.Scheme}://{request.Host}/Song/GetFile?id={song.Id}" : url.AbsoluteUri;
                completeSongs.Add(completeSong);
            }
            return completeSongs;
        }

        private SongProperty GetSongProperty(Song song, Property property, TagLib.File file)
        {
            SongProperty result = new SongProperty { SongId = song.Id, PropertyId = property.Id };

            switch (property.Name)
            {
                case "ALBUM":
                    result.Value = file.Tag.Album;
                    break;
                case "ALBUM_ARTIST":
                    result.Value = file.Tag.FirstAlbumArtist;
                    break;
                case "ARTIST":
                    result.Value = file.Tag.FirstPerformer;
                    break;
                case "BIT_RATE":
                    result.Value = file.Properties != null ? file.Properties.AudioBitrate.ToString() : "0";
                    break;
                case "COMPOSER":
                    result.Value = file.Tag.FirstComposer;
                    break;
                case "DATE_ADDED":
                    result.Value = DateTime.Now.ToString();
                    break;
                case "DESCRIPTION":
                    result.Value = file.Tag.Description;
                    break;
                case "DISC_NUMBER":
                    result.Value = file.Tag.Disc.ToString();
                    break;
                case "GENRE":
                    result.Value = file.Tag.FirstGenre;
                    break;
                case "GROUPING":
                    result.Value = file.Tag.Grouping;
                    break;
                case "HAS_ARTWORK":
                    result.Value = (file.Tag.Pictures.Length > 0).ToString();
                    break;
                case "NAME":
                    result.Value = file.Tag.Title != null ? file.Tag.Title : Path.GetFileNameWithoutExtension(file.Name);
                    break;
                case "SAMPLE_RATE":
                    result.Value = file.Properties != null ? file.Properties.AudioSampleRate.ToString() : "0";
                    break;
                case "TRACK_NUMER":
                    result.Value = file.Tag.Track.ToString();
                    break;
                case "URL":
                    result.Value = song.Url;
                    break;
                case "YEAR":
                    result.Value = file.Tag.Year.ToString();
                    break;
                case "LYRICS":
                    result.Value = file.Tag.Lyrics;
                    break;
                case "MIME":
                    result.Value = file.MimeType;
                    break;
            }
            return result;
        }

        public CompleteSong GetFile(int id)
        {
            try
            {
                Song model = base.Get(id);

                var properties = (from property in SongPropertyTable
                                  join p in PropertyTable on property.PropertyId equals p.Id
                                  orderby property.Id
                                  where property.SongId == id && p.Name == "NAME"
                                  select new CompleteSongProperty
                                  {
                                      Id = property.Id,
                                      Creation = property.Creation,
                                      LastModification = property.LastModification,
                                      PropertyId = p.Id,
                                      Name = p.Name,
                                      Type = p.Type,
                                      Description = p.Description,
                                      SongId = property.SongId,
                                      Value = property.Value
                                  }).ToList();

                Uri url = new Uri(model.Url);
                Stream songStream = null;

                if (url.Scheme == Uri.UriSchemeFile)
                    songStream = new StreamReader(model.Url).BaseStream;
                else if (url.Scheme == Uri.UriSchemeHttp)
                    songStream = new HttpClient().GetStreamAsync(url).Result;

                string contentType = new FileExtensionContentTypeProvider().TryGetContentType(url.AbsoluteUri, out contentType) ?
                    contentType : "application/octet-stream";

                CompleteSong result = new CompleteSong() { Song = model, Type = contentType, File = songStream, Properties = properties };
                return result;
            }
            catch { return null; }
        }
    }
}