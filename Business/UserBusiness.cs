using System.Linq;
using PlaylistAPI.Models;
using PlaylistAPI.Services;

namespace PlaylistAPI.Business
{
    public class UserBusiness : BaseBusiness<User>
    {
        private ICryptographyService CryptographyService { get; }

        public UserBusiness(PlaylistContext context, ICryptographyService cryptographyService) : base(context)
        {
            CryptographyService = cryptographyService;
        }

        public override User Insert(User model)
        {
            model.Password = CryptographyService.GetSHA256(model.Password);
            return base.Insert(model);
        }

        public User UserByName(string name)
        {
            try
            {
                return (from user in Table where user.Name == name select user).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
    }
}