-- Drop tables
drop table if exists "SESSIONS";
drop table if exists "PLAYLIST_SONGS";
drop table if exists "SONG_PROPERTIES";
drop table if exists "RULE_TREES";
drop table if exists "RULES";
drop table if exists "COMPARATORS";
drop table if exists "PROPERTIES";
drop table if exists "PLAYLISTS";
drop table if exists "TREE_OPERATORS";
drop table if exists "TYPES";
drop table if exists "OPERATORS";
drop table if exists "SONGS";
drop table if exists "USERS";

-- Create tables
create table "USERS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"NAME" varchar(255) unique not null,
	"EMAIL" varchar(255) null,
	"PASSWORD" varchar(255) not null
);

create table "SONGS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
    "URL" varchar not null
);

create table "OPERATORS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"DATA" varchar(2) not null,
	"DESCRIPTION" varchar null
);

create table "TYPES" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"DATA" varchar(255) not null,
	"DESCRIPTION" varchar null
);

create table "TREE_OPERATORS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"DATA" varchar(255) not null,
	"DESCRIPTION" varchar null
);

create table "PLAYLISTS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"NAME" varchar(255) not null,
	"USER_ID" integer not null references "USERS"("ID") on delete cascade,
    "SMART" boolean not null
);

create table "PROPERTIES" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
    "NAME" varchar(255) not null,
	"TYPE_ID" integer not null references "TYPES"("ID") on delete cascade,
	"DESCRIPTION" varchar null
);

create table "COMPARATORS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"TYPE_ID" integer not null references "TYPES"("ID") on delete cascade,
    "OPERATOR_ID" integer not null references "OPERATORS"("ID") on delete cascade,
	"DESCRIPTION" varchar null
);

create table "RULES" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"PLAYLIST_ID" integer not null references "PLAYLISTS"("ID") on delete cascade,
	"PROPERTY_ID" integer not null references "PROPERTIES"("ID") on delete cascade,
	"COMPARATOR_ID" integer not null references "COMPARATORS"("ID") on delete cascade,
	"DATA" varchar null,
	"DEPTH" integer not null,
	"VALID" boolean not null -- defined by trigger
);

create table "RULE_TREES" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"PLAYLIST_ID" integer not null references "PLAYLISTS"("ID") on delete cascade,
	"DEPTH" integer not null,
	"TREE_OPERATOR_ID" integer not null references "TREE_OPERATORS"("ID") on delete cascade,
	"PARENT_ID" integer null references "TREE_OPERATORS"("ID") on delete cascade
);

create table "SONG_PROPERTIES" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
	"PROPERTY_ID" integer not null references "PROPERTIES"("ID") on delete cascade,
    "SONG_ID" integer not null references "SONGS"("ID") on delete cascade,
    "DATA" varchar
);

create table "PLAYLIST_SONGS" (
	"ID" serial primary key,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
    "PLAYLIST_ID" integer not null references "PLAYLISTS"("ID") on delete cascade,
    "SONG_ID" integer not null references "SONGS"("ID") on delete cascade
);

create table "SESSIONS" (
	"ID" serial primary key,
	"GUID" varchar(36) not null,
	"CREATION" timestamp not null,
	"LAST_MODIFICATION" timestamp null,
    "USER_ID" integer not null references "USERS"("ID") on delete cascade,
	"USER_NAME" varchar(255) not null references "USERS"("NAME") on delete cascade,
	"USER_AGENT" text null,
    "EXPIRES" timestamp not null
);