-- Default values for TYPES

INSERT INTO "TYPES" ("ID", "DATA", "DESCRIPTION") VALUES(1, 'STRING', 'String of characters');
INSERT INTO "TYPES" ("ID", "DATA", "DESCRIPTION") VALUES(2, 'INTEGER', 'Integer number');
INSERT INTO "TYPES" ("ID", "DATA", "DESCRIPTION") VALUES(3, 'BOOLEAN', 'True of false');
INSERT INTO "TYPES" ("ID", "DATA", "DESCRIPTION") VALUES(4, 'DATETIME', 'Date and time information');

-- Default values for OPERATORS

insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (1, '>', 'greater than');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (2, '>=', 'greater than or equals');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (3, '<', 'smaller than');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (4, '<=', 'smaller than or equals');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (5, '==', 'equals');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (6, '!=', 'not equals');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (7, 'c', 'contains');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (8, '!c', 'does not contain');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (9, 't', 'is true');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (10, '!t', 'is false');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (11, 'b', 'is before');
insert into "OPERATORS" ("ID", "DATA", "DESCRIPTION") values (12, '!b', 'is after');

-- Default values for TREE_OPERATORS
insert into "TREE_OPERATORS" ("ID", "DATA", "DESCRIPTION") values (1, 'ALL', 'All');
insert into "TREE_OPERATORS" ("ID", "DATA", "DESCRIPTION") values (2, 'ANY', 'Any');

-- Default values for COMPARATORS

insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (1, 1, 5, 'String equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (2, 1, 6, 'String not equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (3, 1, 7, 'String contains');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (4, 1, 8, 'String does not contain');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (5, 2, 1, 'Integer greater than');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (6, 2, 2, 'Integer greater than or equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (7, 2, 3, 'Integer smaller than');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (8, 2, 4, 'Integer smaller than or equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (9, 2, 5, 'Integer equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (10, 2, 6, 'Integer not equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (11, 3, 9, 'Boolean is true');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (12, 3, 10, 'Boolean is false');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (13, 4, 5, 'Datetime equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (14, 4, 6, 'Datetime not equals');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (15, 4, 11, 'Datetime is before');
insert into "COMPARATORS" ("ID", "TYPE_ID", "OPERATOR_ID", "DESCRIPTION") values (16, 4, 12, 'Datetime is after');

-- Default values for PROPERTIES

insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (1, 'ALBUM', 1, 'Album');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (2, 'ALBUM_ARTIST', 1, 'Album artist');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (3, 'ARTIST', 1, 'Artist');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (4, 'BIT_RATE', 2, 'Bit rate');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (5, 'CATEGORY', 1, 'Category');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (6, 'COMPOSER', 1, 'Composer');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (7, 'DATE_ADDED', 4, 'Date added');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (8, 'DATE_MODIFIED', 4, 'Date modified');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (9, 'DESCRIPTION', 1, 'Description');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (10, 'DISC_NUMBER', 2, 'Disc number');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (11, 'GENRE', 1, 'Genre');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (12, 'HAS_ARTWORK', 3, 'Has artwork');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (13, 'NAME', 1, 'Title');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (14, 'SAMPLE_RATE', 2, 'Sample rate');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (15, 'SIZE', 2, 'File size');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (16, 'TRACK_NUMER', 2, 'Track number');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (17, 'YEAR', 2, 'Year');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (18, 'LYRICS', 1, 'Lyrics');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (19, 'MIME', 1, 'MIME Type');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (20, 'GROUPING', 1, 'Grouping');
insert into "PROPERTIES" ("ID", "NAME", "TYPE_ID", "DESCRIPTION") values (21, 'URL', 1, 'Url');