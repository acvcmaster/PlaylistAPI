FROM mcr.microsoft.com/dotnet/core/sdk:3.1

WORKDIR /app

COPY bin/Release/netcoreapp3.1/publish/ /app

# Register APT cache
RUN  echo 'Acquire::http { Proxy "http://10.0.0.37:3142"; };' >> /etc/apt/apt.conf.d/01proxy

# Install dependencies
RUN apt-get update && apt-get install -y ffmpeg locales --no-install-recommends \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Generate and set locales for Unicode support
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

ENV LANG en_US.UTF-8 
ENV LANGUAGE en_US:en 
ENV LC_ALL en_US.UTF-8
ENV ASPNETCORE_URLS http://+:5000

EXPOSE 5000/tcp

ENTRYPOINT ["dotnet", "PlaylistAPI.dll"]