using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using PlaylistAPI.Services;
using PlaylistAPI.Business;

namespace PlaylistAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors((options) =>
            {
                options.AddPolicy("_all", (builder) =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddControllers();
            services.AddEntityFrameworkNpgsql().AddDbContext<PlaylistContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString("PlaylistConnection")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Playlist API", Version = "v2" });
            });

            // Services
            services.AddSingleton<ICryptographyService, CryptographyService>();
            services.AddSingleton<IThumbnailingService, FfmpegThumbnailingService>();
            services.AddSingleton<HttpCrawlerService, HttpCrawlerService>();
            services.AddSingleton<FileSystemCrawlerService, FileSystemCrawlerService>();
            services.AddSingleton<IGuidService, GuidService>();

            // Businesses
            services.AddScoped<HardCodedEntryBusiness, HardCodedEntryBusiness>();
            services.AddScoped<PlaylistBusiness, PlaylistBusiness>();
            services.AddScoped<PlaylistRuleBusiness, PlaylistRuleBusiness>();
            services.AddScoped<SongBusiness, SongBusiness>();
            services.AddScoped<UserBusiness, UserBusiness>();
            services.AddScoped<SessionBusiness, SessionBusiness>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Playlist API");
            });

            app.UseRouting();

            app.UseCors("_all");
    
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
