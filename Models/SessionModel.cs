using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlaylistAPI.Models
{
    [Table("SESSIONS")]
    public class Session : BaseModel
    {
        [Column("GUID"), MaxLength(36)]
        public string Guid { get; set; }

        [Column("USER_ID")]
        public int UserId { get; set; }

        [Column("USER_NAME"), Required]
        public string UserName { get; set; }

        [Column("USER_AGENT")]
        public string UserAgent { get; set; }

        [NotMapped, Required]
        public string Password { get; set; }

        [Column("EXPIRES")]
        public DateTime Expires { get; set; }
    }
}

// create table "SESSIONS" (
// 	"ID" serial primary key,
// 	"GUID" varchar(36) not null,
// 	"CREATION" timestamp not null,
// 	"LAST_MODIFICATION" timestamp null,
//     "USER_ID" integer not null references "USERS"("ID") on delete cascade,
// 	"USER_NAME" varchar(255) not null references "USERS"("NAME") on delete cascade,
//     "EXPIRES" timestamp not null
// );