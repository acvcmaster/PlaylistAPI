using System;
using System.IO;
using System.Net.Http;

using static TagLib.File;

namespace PlaylistAPI.Models
{
    public class TaglibFile : IFileAbstraction
    {
        public Uri Url { get; }

        public string Name { get; }

        private Stream stream = new MemoryStream();

        private HttpClient client;

        public TaglibFile(string name, Uri url)
        {
            Name = name;
            Url = url;
            client = new HttpClient();
            var httpStream = client.GetStreamAsync(url).Result;
            httpStream.CopyTo(stream);
        }

        public Stream ReadStream => stream;

        public Stream WriteStream => stream;

        public void CloseStream(Stream stream) { this.stream.Close(); }
    }
}