namespace PlaylistAPI.Models
{
    public class AmplitudeJSSong
    {
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Url { get; set; }
        public string Cover_art_url { get; set; }
        public string Lyrics { get; set; }
    }
}