using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace PlaylistAPI.Services
{
    public interface ICrawlerService
    {
        IEnumerable<string> ListFiles(string path);
    }

    public class HttpCrawlerService : ICrawlerService
    {
        public HttpCrawlerService()
        {
            fileMatcher = new Regex("<a href=\"(?<name>.*[^/])\">.*</a>");
            directoryMatcher = new Regex("<a href=\".*\">(?<name>[^../].*/)</a>");
        }

        private Regex directoryMatcher { get; }

        private Regex fileMatcher { get; }

        public IEnumerable<string> ListFiles(string path)
        {
            try { return ListFiles(path, true); }
            catch { return null; }
        }
        public IEnumerable<string> ListFiles(string path, bool all)
        {
            if (!all)
            {
                string url = path;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string html = reader.ReadToEnd();
                        MatchCollection matches = fileMatcher.Matches(html);
                        if (matches.Count > 0)
                            foreach (Match match in matches)
                                if (match.Success)
                                {
                                    var value = match.Groups["name"].Value;
                                    yield return Combine(url, value);
                                }
                    }
                }
            }
            else
            {
                var dirs = ListDirectories(path, true);
                
                foreach (var f in ListFiles(path, false))
                    yield return f;

                foreach (var d in dirs)
                    foreach (var f in ListFiles(d, false))
                        yield return f;
            }
        }

        /// <summary>
        /// List directories on remote (http) server.
        /// </summary>
        /// <param name="path">Root directory</param>
        /// <returns>List of directories</returns>
        public IEnumerable<string> ListDirectories(string path, bool recursive)
        {
            var url = path;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string html = reader.ReadToEnd();
                    MatchCollection matches = directoryMatcher.Matches(html);
                    if (matches.Count > 0)
                        foreach (Match match in matches)
                            if (match.Success)
                            {
                                var value = match.Groups["name"].Value;
                                yield return Combine(path, value);
                                if (recursive)
                                {
                                    foreach (string s in ListDirectories(Combine(path, value), true))
                                        yield return s;
                                }
                            }
                }
            }
        }

        private static string Combine(string uri1, string uri2)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }
    }

    public class FileSystemCrawlerService : ICrawlerService
    {
        public IEnumerable<string> ListFiles(string path)
        {
            try { return Directory.GetFiles(path, "*", SearchOption.AllDirectories); }
            catch { return null; }
        }
    }
}