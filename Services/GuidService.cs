using System;

namespace PlaylistAPI.Services
{
    public interface IGuidService
    {
        string GetGuid();
    }
    public class GuidService : IGuidService
    {
        public string GetGuid() => Guid.NewGuid().ToString().ToUpperInvariant();
    }
}