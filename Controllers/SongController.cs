using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PlaylistAPI.Business;
using PlaylistAPI.Models;

namespace PlaylistAPI.Controllers
{
    [Route("[controller]/[action]")]
    public class SongController : BaseController<Song, SongBusiness>
    {
        private SongBusiness Business { get; }

        public SongController(PlaylistContext context, SongBusiness business) : base(context, business)
        {
            Business = business;
        }

        [HttpGet]
        [Produces("application/octet-stream", "application/json")]
        public IActionResult GetFile([FromQuery]int id)
        {
            var file = Business.GetFile(id);
            if (file != null)
            {
                return File(file.File, file.Type, file.Properties.FirstOrDefault().Value, true);
            }
            return NotFound("Could not get song file by id.");
        }

        [HttpGet]
        [Produces("image/jpeg", "application/json")]
        public IActionResult GetCoverArt([FromQuery]int id)
        {
            var result = Business.GetCoverArt(id);
            if (result != null)
                return File(result, "image/jpeg", true);
            return NotFound("Could not get cover art by song id.");
        }

        [HttpPost]
        public IActionResult MassInsert([FromQuery]string directoryUrl)
        {
            var result = Business.MassInsert(directoryUrl);
            if (result != null)
                return Ok(result);
            return BadRequest("Failed to mass insert songs (directory exists?).");
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery]int page, [FromQuery]int entriesPerPage)
        {
            var result = Business.GetAll(page, entriesPerPage, this.HttpContext.Request);
            if (result != null)
                return Ok(JsonConvert.SerializeObject(result, Formatting.Indented));
            return BadRequest("Could not get songs.");
        }
    }
}