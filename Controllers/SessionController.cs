using Microsoft.AspNetCore.Mvc;
using PlaylistAPI.Business;
using PlaylistAPI.Models;

namespace PlaylistAPI.Controllers
{
    [Route("[controller]/[action]")]
    public class SessionController : BaseController<Session, SessionBusiness>
    {
        public SessionBusiness Business { get; }

        public SessionController(PlaylistContext context, SessionBusiness business) : base(context, business)
        {
            Business = business;
        }

        [HttpPost]
        public IActionResult Login([FromBody]Session model)
        {
            var result = Business.Login(model);
            return Ok(result);
        }
    }
}