using Microsoft.AspNetCore.Mvc;
using PlaylistAPI.Business;
using PlaylistAPI.Models;

namespace PlaylistAPI.Controllers
{
    [ApiController]
    public abstract class BaseController<TModel, TBusiness> : ControllerBase
        where TModel : BaseModel
        where TBusiness : BaseBusiness<TModel>
    {
        private PlaylistContext Context { get; }
        private TBusiness Business { get; }

        public BaseController(PlaylistContext context, TBusiness business)
        {
            Context = context;
            Business = business;
        }

        [HttpGet]
        public virtual IActionResult Get([FromQuery]int id)
        {
            try
            {
                var result = Business.Get(id);
                if (result != null)
                    return Ok(result);
                return NoContent();
            }
            catch { return BadRequest($"Could not get {typeof(TModel).Name} by id."); }
        }

        [HttpPost]
        public virtual IActionResult Insert([FromBody]TModel model)
        {
            if (!ModelState.IsValid)
                return ValidationProblem();

            var result = Business.Insert(model);
            if (result != null)
                return Ok(result);
            return BadRequest($"Could not insert {typeof(TModel).Name} on database.");
        }

        [HttpPut]
        public virtual IActionResult Update([FromBody]TModel model)
        {
            if (!ModelState.IsValid)
                return ValidationProblem();

            var result = Business.Update(model);
            if (result != null)
                return Ok(result);
            return BadRequest($"Could not update {typeof(TModel).Name} on database.");
        }

        [HttpDelete]
        public virtual IActionResult Delete([FromQuery]int id)
        {
            var result = Business.Delete(id);
            if (result != null)
                return Ok(result);
            return BadRequest($"Could not delete {typeof(TModel).Name} from database.");
        }
    }
}